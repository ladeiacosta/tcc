\babel@toc {brazilian}{}
\babel@toc {english}{}
\babel@toc {brazilian}{}
\contentsline {chapter}{1~Introdu\c c\~ao}{12}{CHAPTER.1}
\contentsline {section}{\numberline {1.1}Problema de Pesquisa}{14}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{14}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivo Geral}{14}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{14}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Organiza\c c\~ao do Texto}{15}{section.1.3}
\contentsline {chapter}{2~Referencial Te\'orico}{16}{CHAPTER.2}
\contentsline {section}{\numberline {2.1}Amputa\c c\~ao}{16}{section.2.1}
\contentsline {section}{\numberline {2.2}Motiva\c c\~ao}{19}{section.2.2}
\contentsline {section}{\numberline {2.3}Jogos, Gamifica\c c\~ao e Jogos S\'erios}{23}{section.2.3}
\contentsline {section}{\numberline {2.4}Realidade Virtual}{27}{section.2.4}
\contentsline {section}{\numberline {2.5}Inform\'atica M\'edica}{28}{section.2.5}
\contentsline {section}{\numberline {2.6}Microcontroladores}{29}{section.2.6}
\contentsline {section}{\numberline {2.7}Trabalhos Correlatos}{30}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Running Wheel}{30}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}CyberCycle}{32}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}Ikapp}{33}{subsection.2.7.3}
\contentsline {subsection}{\numberline {2.7.4}Jogo Para Reabilita\c c\~ao de Cadeirantes}{35}{subsection.2.7.4}
\contentsline {subsection}{\numberline {2.7.5}Upper Limb Rehabilitation Game}{36}{subsection.2.7.5}
\contentsline {subsection}{\numberline {2.7.6}Considera\c c\~oes Finais}{37}{subsection.2.7.6}
\contentsline {section}{\numberline {2.8}Projeto Victus}{40}{section.2.8}
\contentsline {chapter}{3~Metodologia}{43}{CHAPTER.3}
\contentsline {chapter}{4~Projeto - Victus VR}{45}{CHAPTER.4}
\contentsline {section}{\numberline {4.1}Descri\c c\~ao}{45}{section.4.1}
\contentsline {section}{\numberline {4.2}Modelagem e Concep\c c\~ao}{47}{section.4.2}
\contentsline {section}{\numberline {4.3}Desenvolvimento}{48}{section.4.3}
\contentsline {section}{\numberline {4.4}Teste e Valida\c c\~ao}{58}{section.4.4}
\contentsline {chapter}{5~Resultados e Conclus\~oes}{61}{CHAPTER.5}
\contentsline {chapter}{\xspace {}Refer{\^e}ncias}{64}{schapter.8}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} A~--- Documento de Requisitos e Descri\c c\~ao Textual do Software}{68}{CHAPTER.A}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} B~--- Sprints}{73}{CHAPTER.B}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} C~--- Casos de Uso}{75}{CHAPTER.C}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} D~--- \textit {Game Design Document}}{77}{CHAPTER.D}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} E~--- Playtest - Victus VR}{83}{CHAPTER.E}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} F~--- Question\'ario do Coeficiente Funcional}{86}{CHAPTER.F}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} G~--- Flow}{89}{CHAPTER.G}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} H~--- Question\'ario de Avalia\c c\~ao - Fisioterapeutas}{91}{CHAPTER.H}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} I~--- Protocolo de Experimento}{93}{CHAPTER.I}
